from __future__ import print_function
from flask import Flask
from flask import request, jsonify, make_response
import requests as req
import os,re,sys,bisect,hashlib,json,copy,time,threading,sched,logging
app = Flask(__name__)

TIMEOUT = 5

def positiveHash(toHash):
	return hash(toHash)%((sys.maxsize+1)*2)
	
#heal function
def heal():
	global valmap
	global healThread
	#communicate within partition
	for i in range(partition_id*k,partition_id*k+k):
		try:
			#for any non-self node
			if i != view_id:
				#try to access their dictionary
				url_str = 'http://' + view[i] + '/kvs/update_view'
				resp = req.get(url_str,timeout=3).content
				#load values into dictionary
				comparisonFrom = json.loads(resp)
				#compare timestamps of keys
				if int(len(comparisonFrom)) == int(len(valmap)):
					for key,val in valmap.items():
						try:
							timestampComparison1 = str(comparisonFrom[key]).split(':')
							timestampComparison2 = str(valmap[key]).split(':')
						except:
							print(comparisonFrom[key],file=sys.stderr)
							print(valmap[key],file=sys.stderr)
						#update entries to their latest values
						try:
							if int(timestampComparison1[1]) > int(timestampComparison2[1]):
								try:
									valmap[key] = comparisonFrom[key]
								except:
									print('failure in assignment', file=sys.stderr)
						except:
							print('failure in comparison', file=sys.stderr)
				#else decide by view_id priority
				else:
					if view_id > i:
					#outright replace? haven't decided yet
						valmap = copy.deepcopy(comparisonFrom)
		except:
			pass
	healThread = threading.Timer(4.0,heal, ())
	healThread.start()
	
#setup
valmap = {}
ip_port = str(os.environ.get('IPPORT'))
k = int(os.environ.get('K'))
healThread = threading.Thread()

if os.environ.has_key('VIEW'):
	view = os.environ.get('VIEW').split(',')
	view_id = view.index(ip_port)
	partition_id = view_id / k
	causal_history = [0] * len(view)
else:
	view = list()
	view_id = int()
	partition_id = int()
	causal_history = list()

#adjust log
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.DEBUG)

app.logger.debug("Hello World!")
	
@app.route('/test',methods=['GET'])
def test():
	return make_response(jsonify({'view_id':view_id,'ip_port':ip_port,'views':str(view),'partition_id':partition_id,'k':k,'vector_clock':'.'.join(map(str,causal_history)),'dict':valmap}),200)
	
@app.route('/kvs/update_view',methods=['PUT','GET'])
def update_view():
	if request.method == 'PUT':
		global view
		global view_id
		global partition_id
		global causal_history
		req_type = request.args.get('type')
		final_destination = str(request.form.get('ip_port'))
		if req_type == 'add':
			#step 1: propagate view
			#update new node
			url_str = 'http://' + final_destination + '/kvs/update_view?type=new'
			req.put(url_str,data={'ip_ports':str(','.join(view)),'payload':final_destination},timeout=TIMEOUT)
			#update everyone else but self
			for i in range(len(view)):
				try:
					if view[i] != ip_port:
						url_str = 'http://' + view[i] + '/kvs/update_view?type=new'
						req.put(url_str,data={'ip_ports':str(','.join(view)),'payload':final_destination},timeout=TIMEOUT)
				except:
					pass
			#update self
			url_str = 'http://' + ip_port + '/kvs/update_view?type=new'
			req.put(url_str,data={'ip_ports':str(','.join(view)),'payload':str(final_destination)},timeout=TIMEOUT)
			
			#step 2: signal redistribution
			for i in range(len(view)):
				try:
					url_str = 'http://' + view[i] + '/kvs/update_view?type=redistribute'
					req.put(url_str,timeout=TIMEOUT)
				except:
					pass
			#step 3: return
			return make_response(jsonify({'msg':'success','partition_id':(len(view)-1)/k,"number_of_partitions":((len(view)-1)/k)+1}),200)

		elif req_type == 'remove':
			#step 1: try to retrieve to-delete's keys
			try:
				url_str = 'http://' + final_destination + '/kvs/update_view'
				resp = req.get(url_str,timeout=TIMEOUT).content
				adding = json.loads(resp)
				valmap.update(adding)
			except:
				pass
			#step 2: update view
			index_to_remove = view.index(final_destination)
			view.remove(final_destination)
			#step 3: propagate view
			for i in range(len(view)):
				try:
					url_str = 'http://' + view[i] + '/kvs/update_view?type=delete'
					req.put(url_str,data={'ip_ports':str(','.join(view)),'payload':str(index_to_remove)},timeout=TIMEOUT)
				except:
					pass

			#step 4: signal redistribution
			for i in range(len(view)):
				try:
					url_str = 'http://' + view[i] + '/kvs/update_view?type=redistribute'
					req.put(url_str,timeout=TIMEOUT)
				except:
					pass
			return make_response(jsonify({'msg':'success',"number_of_partitions":((len(view)-1)/k)+1}),200)

			
		#redistribute
		elif req_type == 'redistribute':
			#empty kvs
			redistribute = copy.deepcopy(valmap)
			valmap.clear()
			#redistribute kvs
			for i in range(len(redistribute)):
				itemDest = positiveHash(redistribute.keys()[i])%len(view)
				itemData = redistribute.values()[i].split(':')
				url_str = 'http://' + view[itemDest] + '/kvs/' + redistribute.keys()[i]
				req.put(url_str,data={'val':itemData[0]},timeout=TIMEOUT)
					
			return make_response(jsonify({'msg':'success'}),200)
			
		#reinitialize a view	
		elif req_type == 'new':
			#update view
			view = str(request.form.get('ip_ports')).split(',')
			payload = str(request.form.get('payload'))
			view.append(payload)
			#update view_id
			view_id = view.index(ip_port)
			partition_id = view_id / k
			#generate new causal history
			if ip_port == payload:
				causal_history = [0] * len(view)
			else:
				causal_history.append(0)
				
			return make_response(jsonify({'msg':'success'}),200)
		#delete a node
		elif req_type == 'delete':
			#update view
			view = str(request.form.get('ip_ports')).split(',')
			payload = int(request.form.get('payload'))
			#update view_id
			view_id = view.index(ip_port)
			partition_id = view_id/k
			del causal_history[payload]
			
			return make_response(jsonify({'msg':'success'}),200)
			
		elif req_type == 'replace':
			key = str(request.form.get('key'))
			val = str(request.form.get('val'))
			valmap[key] = val
			return make_response(jsonify({'msg':'success'}),200)
		
		#invalid request
		else:
			return make_response(jsonify({'error':'invalid type of request for \'update_view\''}), 400)
			
	elif request.method == 'GET':
		data = jsonify(valmap)
		return make_response(data,200)
		#return dictionary as json
	
	
@app.route('/kvs/',methods=['GET','PUT'])
def nolen(): #length is 0, undefined key
	return jsonify({'error':'invalid length','msg':'error'}),400	
	
#kvs GET PUT DELETE
@app.route('/kvs/<key>',methods=['GET','PUT'])
def kvs(key):
	if re.match("[^a-zA-Z0-9_]",key): #alphanumeric, _ only
		return make_response(jsonify({'error':'invalid charset','msg':'error'}),400)	
	if not 1 <= len(key) <= 250: #length [1,250]
		return make_response(jsonify({'error':'invalid length','msg':'error'}),400)
	#PUT	
	if request.method == 'PUT':	
		#precondition
		if not request.form.get('val'): #val='value' format
			return jsonify({'error':'invalid format','msg':'error'}),400
		if sys.getsizeof(request.form.get('val')) > 1500000: #size < 1.5 MB
			return make_response(jsonify({'error':'invalid size','msg':'error'}),400)
			
		causal_payload = request.form.get('causal_payload')
		#synchronize clock
		if request.form.get('causal_payload'):
			causal_payload =  map(int,request.form.get('causal_payload').split('.'))
			if len(causal_history) == len(causal_payload):
				for i in range(len(causal_history)):
					causal_history[i] = max(causal_payload[i],causal_history[i])
		#increase clock
		causal_history[view_id] += 1	
		#hash
		hash = positiveHash(key) % (len(view)/k)
		#check correct partition set
		if hash == partition_id:
			cur_status = 201
			timestamp = int(time.time())
			for i in range(partition_id*k,partition_id*k+k):
				try:
					url_str = 'http://' + view[i] + '/kvsadd/'
					resp = req.put(url_str,data={'val':request.form.get('val'),'key':key,'timestamp':timestamp,'causal_payload':str('.'.join(map(str,causal_history)))},timeout=TIMEOUT)
					if resp.status_code == 200:
						cur_status = 200
					#synchronize clocks
					data = resp.json()
					get = data.get('causal_payload').split('.')
					update = map(int,get)
					for i in range(len(causal_history)):
						causal_history[i] = max(causal_history[i],update[i])
				except:
					pass
			causal_history[view_id] += 1	
			return make_response(jsonify({'msg':'success','partition_id':partition_id,'causal_payload':'.'.join(map(str,causal_history)),'timestamp':timestamp}),cur_status)
		#if not redirect set
		else:
			for i in range(hash*k,(hash*k)+k):
				try:
					url_str = 'http://' + view[i] + '/kvs/' + key
					resp = req.put(url_str, data={'val':request.form.get('val'),'causal_payload':str('.'.join(map(str,causal_history)))},timeout=TIMEOUT)
					return make_response(resp.content,resp.status_code)
				except:
					pass
			return make_response(jsonify({'error':'service not available','msg':'error'}),404)
				
	#GET			
	elif request.method == 'GET':
		#synchronize clock
		if request.form.get('causal_payload'):
			causal_payload =  map(int,request.form.get('causal_payload').split('.'))
			if len(causal_history) == len(causal_payload):
				for i in range(len(causal_history)):
					causal_history[i] = max(causal_payload[i],causal_history[i])
		#hash key	
		hash = positiveHash(key) % (len(view)/k)
		#check correct partition set
		if hash == partition_id:
			#for each partition,
			vectorClockCurrent = -1
			valueToReturn = None
			timestampToReturn = -1
			#try all available nodes
			for i in range(partition_id*k,partition_id*k+k):
				try:
					#get key value:timestamp, vector clock
					url_str = 'http://' + view[i] + '/kvsget/'
					resp = req.get(url_str, data={'key':key})
					data = resp.json()
					#split value and timestamp, save incoming vector clock
					message = data.get('msg')
					#if data found
					if message == 'success':
						#unpack message data
						incoming = data.get('val')
						compare = incoming.split(':')
						#unpack vector clock data
						vectorClock = data.get('causal_history').split('.')
						vectorClockInt = map(int,vectorClock)
						if vectorClockInt[view_id] > vectorClockCurrent:
							valueToReturn = compare[0]
							timestampToReturn = compare[1]
							vectorClockCurrent = vectorClockInt[view_id]
						elif vectorClockInt[view_id] == vectorClockCurrent:
							if int(compare[1]) > timestampToReturn:
								valueToReturn = compare[0]
								timestampToReturn = compare[1]
				except:
					pass
			#return latest value
			causal_history[view_id] += 1
			if valueToReturn and timestampToReturn: #check for existence
				return make_response(jsonify({'msg':'success','partition_id':partition_id,'value':valueToReturn,'timestamp':timestampToReturn,'causal_payload':str('.'.join(map(str,causal_history)))}))
			else: #non-existent key
				return make_response(jsonify({'msg':'error','partition_id':partition_id,'error':'key does not exist','val':valueToReturn,'t':timestampToReturn}))
		else:
			for i in range(hash*k,(hash*k)+k):
				try:
					url_str = 'http://' + view[i] + '/kvs/' + key
					resp = req.get(url_str,timeout=TIMEOUT+1)
					return make_response(resp.content,resp.status_code)
				except:
					print('failed to access ' + str(view[i]),file=sys.stderr)		
			return make_response(jsonify({'error':'service not available','msg':'error'}),404)

#kvsadd
@app.route('/kvsadd/',methods=['PUT'])
def kvsadd():
	global view_id
	val = request.form.get('val')
	key = request.form.get('key')
	timestamp = int(request.form.get('timestamp'))
	causal_payload = map(int,str(request.form.get('causal_payload')).split('.'))
	
	#update vector clock
	for i in range(len(causal_history)):
		causal_history[i] = max(causal_payload[i],causal_history[i])
		
	causal_history[view_id] += 1
	
	#update value
	valmap[key] = val + ':' + str(timestamp)
	return make_response(jsonify({'msg':'success','replaced':1,'causal_payload':'.'.join(map(str,causal_history))}),200)
	
#kvsget
@app.route('/kvsget/',methods=['GET'])
def kvsget():
	key = request.form.get('key')
	if key in valmap:
		return make_response(jsonify({'msg':'success','val':valmap[key],'causal_history':'.'.join(map(str,causal_history))}))
	else:
		return make_response(jsonify({'msg':'failure'}))
	
#get_partition_id
@app.route('/kvs/get_partition_id',methods=['GET'])
def partitionID():
	return make_response(jsonify({'msg':'success','partition_id':partition_id}),200)
	
#get_all_partition_id
@app.route('/kvs/get_all_partition_ids',methods=['GET'])
def allPartitionID():
	return make_response(jsonify({'msg':'success','partition_id_list':range(0,len(view)/k)}),200)

#get_partition_members
@app.route('/kvs/get_partition_members',methods=['GET'])
def allPartitionMembers():
	id = int(request.form.get('partition_id'))
	return make_response(jsonify({'msg':'success','partition_members':view[id*k:id*k+k]}),200)
	
@app.before_first_request
def start_thread():
	healThread = threading.Timer(4.0,heal,())
	healThread.start()

#define port
if __name__ == '__main__':
	port = int(os.environ.get('PORT',8080)) #define port 8080
	app.run(host='0.0.0.0',port=port,threaded=True) # localhost
